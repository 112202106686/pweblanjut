<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Dosen</title>
	<link rel="stylesheet" type="text/css" href="bootstrap4/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/styleku.css">
	<script src="bootstrap4/jquery/3.3.1/jquery-3.3.1.js"></script>
	<script src="bootstrap4/js/bootstrap.js"></script>
	<!-- Use fontawesome 5-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body>
    <div class="utama">
    <h2 class="text-center">Data Dosen</h2>
    <a href="addDosen.php" class="btn btn-primary">Tambah Dosen</a>
    <span class="float-right">
		<form action="" method="post" class="form-inline">
			<button class="btn btn-success" type="submit">Cari</button>
			<input class="form-control mr-2 ml-2" type="text" name="cari" placeholder="cari data dosen..." autofocus autocomplete="off">
		</form>
	</span>
	<br><br>
    <?php
    require "fungsi.php";
    require "head.html";

//jumlah data per halaman
$jmlDataPerHal = 3;

//cari jumlah data
if (isset($_POST['cari'])){
	$cari=$_POST['cari'];
	$sql="select * from dosen where npp like'%$cari%' or
						  namadosen like '%$cari%' or
						  homebase like '%$cari%'";
}else{
	$sql="select * from dosen";		
}
$qry = mysqli_query($koneksi,$sql) or die(mysqli_error($koneksi));
$jmlData = mysqli_num_rows($qry);

$jmlHal = ceil($jmlData / $jmlDataPerHal);
if (isset($_GET['hal'])){
	$halAktif=$_GET['hal'];
}else{
	$halAktif=1;
}

$awalData=($jmlDataPerHal * $halAktif)-$jmlDataPerHal;

//Jika tabel data kosong
$kosong=false;
if (!$jmlData){
	$kosong=true;
}
//data berdasar pencarian atau tidak
if (isset($_POST['cari'])){
	$cari=$_POST['cari'];
	$sql="select * from dosen where npp like'%$cari%' or
						  namadosen like '%$cari%' or
						  homebase like '%$cari%'
						  limit $awalData,$jmlDataPerHal";
}else{
	$sql="select * from dosen limit $awalData,$jmlDataPerHal";		
}
//Ambil data untuk ditampilkan
$hasil=mysqli_query($koneksi,$sql) or die(mysqli_error($koneksi));

    $sql = "SELECT * FROM dosen";
    $rs = mysqli_query($koneksi,$sql) OR die(mysqli_error($koneksi));

    if(mysqli_num_rows($rs) == 0)
    {
        echo "data masih kosong";
    }
    else
    {
    ?>

    <!-- Cetak data dengan tampilan tabel -->
	<table class="table table-hover">
	<thead class="thead-light">    
    <table class="table table-bordered table-striped">
        <tr>
            <th>No</th>
            <th>NPP</th>
            <th>Nama</th>
            <th>Homebase</th>
            <th>Aksi</th>
        </tr>
    <?php
        $i = 1;
        while($result = mysqli_fetch_object($rs))
        {
    ?>
        <tr>
            <td><?php echo $i?></td>
            <td><?php echo $result->npp?></td>
            <td><?php echo $result->namadosen?></td>
            <td><?php echo $result->homebase?></td>
            <td>Edit | Delete</td>
        </tr>
    <?php
            $i++;
        }
    }
        ?>
</body>
</html>